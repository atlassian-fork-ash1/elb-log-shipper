# atlassian / elb-log-shipper

service that parses new AWS ELB/ALB access logs from S3 and writes to Kinesis


## Overview

We intend for this project to primarily expose a library,
implementing core, useful functionality

This project includes an [entrypoint](./cmd/elb-log-shipper/main.go) that we intend to be a fully-functioning, working example of how to instantiate / integrate the core library

After reviewing our example main.go,
and perhaps running it yourself,
you should ideally create your own entrypoint,
taking into account your own specific requirements


## Operations

-   configure your ELBs / ALBs so that they save access logs in an S3 Bucket

-   configure an SNS Topic that notifies when this S3 Bucket receives Objects

-   configure an SQS Queue that subscribes to this SNS Topic

-   configure a destination Kinesis Stream

-   configure elb-log-shipper with the above details


## Data Flow

```
   +-------------+
  +-------------+|
 +-------------+||
+-------------+|||      +------------+      +------------+
|             ||||      |            |      |            |
| ELBs / ALBs |||+  1.  | S3 Bucket  |  2.  | SNS Topic  |
|             |--------->            +------>            |
|             ++        |            |      |            |
+-------------+         +-----+------+      +-----+------+
                              |                   |
                              |5.                 |3.
                              |                   |
    +------------+      +-----v------+      +-----v------+
    |            |  6.  |            |  4.  |            |
    | Kinesis    <------+ elb-log-   <------+ SQS Queue  |
    | Stream     |      |   shipper  |      |            |
    |            |      |            |      |            |
    +------------+      +------------+      +------------+

```

1. the S3 Bucket receives access logs for an ELB / ALB

2. the new S3 Object triggers the SNS Topic

3. the SNS Topic notifies and the SQS Queue enqueues a message

4. the elb-log-shipper pulls the message from the SQS Queue

5. the elb-log-shipper retrieves the associated Object from the S3 Bucket,
  retrieves the tags from the associated ELB / ALB,
  and parses the contents of the access log file

6. the elb-log-shipper pushes the result to the Kinesis Stream


## Code of Conduct

Please see [CODE_OF_CONDUCT.md](./CODE_OF_CONDUCT.md)


## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md)


## Documentation

For more information, see the [docs folder](./docs)


## License

We license this project under the [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0),
as per our [LICENSE.txt](./LICENSE.txt)
