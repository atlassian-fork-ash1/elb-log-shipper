package main

import (
	"context"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"os/signal"
	"runtime"
	"strconv"
	"syscall"
	"time"

	"bitbucket.org/atlassian/elb-log-shipper/pkg/logserver"

	"github.com/DataDog/datadog-go/statsd"
	log "github.com/Sirupsen/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials/stscreds"
	"github.com/aws/aws-sdk-go/aws/defaults"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/elb"
	"github.com/aws/aws-sdk-go/service/elbv2"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sns"
	"github.com/aws/aws-sdk-go/service/sqs"
)

const (
	// TODO: get this from environment variables
	appPort           = "8080"
	maxAwsRetries     = 10
	sqsWaitTime       = 20
	sqsMaxMessages    = 10
	logSourceType     = "load_balancer_logs"
	maxKinesisRetries = 3
	statsdAddr        = "statsd:8125"
	datadogNamespace  = "ElbLogShipper."
)

func main() {
	rand.Seed(time.Now().UnixNano())
	log.SetFormatter(&log.JSONFormatter{})
	if err := run(); err != nil && err != context.Canceled {
		log.Fatal(err)
	}
}

func run() error {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	cancelOnInterrupt(ctx, cancel)

	// Open new AWS sessions, implicitly using the default aws.DefaultRetryer which implements exponential backoff for
	// retrying all requests, including Kinesis throttling.
	awsRegion, err := getRequiredEnv("AWS_REGION")
	if err != nil {
		return err
	}

	sess, err := session.NewSession(defaults.Get().Config.WithRegion(awsRegion).WithMaxRetries(maxAwsRetries))
	if err != nil {
		return fmt.Errorf("unable to create session: %v", err)
	}

	kinesisRegion, err := getRequiredEnv("KINESIS_REGION")
	if err != nil {
		return err
	}

	kinesisRoleArn, err := getRequiredEnv("KINESIS_ROLE_ARN")
	if err != nil {
		return err
	}

	sdk := &logserver.SdkContextWrapper{
		ElbService:   elb.New(sess),
		Elbv2Service: elbv2.New(sess),
		KinesisService: kinesis.New(sess, aws.NewConfig().WithRegion(kinesisRegion).
			WithCredentials(stscreds.NewCredentials(sess, kinesisRoleArn))),
		S3Service:  s3.New(sess),
		SqsService: sqs.New(sess),
		SnsService: sns.New(sess),
	}

	queueUrl, err := getRequiredEnv("SQS_LOGFILES_QUEUE_URL")
	if err != nil {
		return err
	}

	queueAttributesOutput, err := sdk.GetQueueAttributes(ctx, &sqs.GetQueueAttributesInput{
		AttributeNames: []*string{aws.String("QueueArn"), aws.String("VisibilityTimeout")},
		QueueUrl:       aws.String(queueUrl),
	})
	if err != nil {
		return err
	}
	queueArn := *queueAttributesOutput.Attributes["QueueArn"]

	visibilityTimeout, err := strconv.ParseInt(*queueAttributesOutput.Attributes["VisibilityTimeout"], 10, 64)
	if err != nil {
		return err
	}

	kinesisStream, err := getRequiredEnv("KINESIS_STREAM_NAME")
	if err != nil {
		return err
	}

	statsClient, err := statsd.New(statsdAddr)
	if err != nil {
		return err
	}
	statsClient.Namespace = datadogNamespace

	loadBalancerTagCacher := logserver.NewLoadBalancerTagCacher()
	loadBalancerTagCacher.CleanupInterval = 30 * time.Minute
	loadBalancerTagCacher.ExpirationTime = 30 * time.Minute

	loadBalancerLogParser := logserver.NewLoadBalancerLogParser()
	loadBalancerLogParser.Statsd = statsClient
	loadBalancerLogParser.LineParser = &logserver.LoadBalancerLogLineParser{}
	loadBalancerLogParser.Workers = runtime.NumCPU()

	logServer := logserver.LogServer{
		Fetcher: &logserver.SQSFetcher{
			S3Service:  sdk,
			SqsService: sdk,
			TagFetcherService: &logserver.LoadBalancerTagFetcher{
				ElbService:   sdk,
				Elbv2Service: sdk,
				Cacher:       loadBalancerTagCacher,
			},
			PollWaitTime:          sqsWaitTime,
			QueueUrl:              queueUrl,
			VisibilityTimeoutSecs: visibilityTimeout,
			MaxMessages:           sqsMaxMessages,
		},
		Parser: loadBalancerLogParser,
		WriterFactory: func(logInfo *logserver.LogFileInfo) logserver.LogEntryWriter {
			return &logserver.KinesisWriter{
				KinesisService:    sdk,
				Statsd:            statsClient,
				LogStreamName:     kinesisStream,
				Environment:       "my-environment",
				ServiceId:         "my-service",
				SourceType:        logSourceType,
				LogInfo:           logInfo,
				MaxRecordDataSize: 300 * 1024,
				MaxShardRetries:   maxKinesisRetries,
			}
		},
		Statsd: statsClient,
	}

	httpMux := http.NewServeMux()
	httpMux.HandleFunc("/heartbeat", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "OK")
	})
	httpServer := &http.Server{
		Addr:    ":" + appPort,
		Handler: httpMux,
	}

	snsTopicArn, err := getRequiredEnv("LOG_SNS_TOPIC_ARN")
	if err != nil {
		return err
	}

	// here we subscribe our SQS queue to the SNS topic which suits our use case
	// alternatively, do this via CloudFormation, etc and simplify your main.go!
	_, err = sdk.Subscribe(ctx, &sns.SubscribeInput{
		Endpoint: aws.String(queueArn),
		Protocol: aws.String("sqs"),
		TopicArn: aws.String(snsTopicArn),
	})
	if err != nil {
		return err
	}

	go func() {
		log.Infof("Listening on port %s", appPort)
		if err := httpServer.ListenAndServe(); err != nil {
			cancel()
			log.Fatal(err)
		}
	}()
	logServer.Run(ctx)
	return nil
}

// Calls f when os.Interrupt or SIGTERM is received.
func cancelOnInterrupt(ctx context.Context, f context.CancelFunc) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)
	go func() {
		select {
		case <-ctx.Done():
		case <-c:
			f()
		}
	}()
}

func getRequiredEnv(key string) (string, error) {
	value, contains := os.LookupEnv(key)
	if !contains {
		return "", fmt.Errorf("required environment variable %s was empty", key)
	}
	return value, nil
}
