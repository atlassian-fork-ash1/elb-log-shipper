package logserver

import (
	"context"
	"io"
	"time"

	"github.com/aws/aws-sdk-go/service/elb"
	"github.com/aws/aws-sdk-go/service/elbv2"
	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type S3 interface {
	GetObject(context.Context, *s3.GetObjectInput) (*s3.GetObjectOutput, error)
}

type SQS interface {
	ReceiveMessage(context.Context, *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error)
	DeleteMessage(context.Context, *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error)
}

type TagFetcher interface {
	FetchTagsElb(context.Context, string) (map[string]string, error)
	FetchTagsElbv2(context.Context, string, string, string) (map[string]string, error)
}

type ElbTagDescriber interface {
	DescribeTagsElb(context.Context, *elb.DescribeTagsInput) (*elb.DescribeTagsOutput, error)
}

type Elbv2TagDescriber interface {
	DescribeTagsElbv2(context.Context, *elbv2.DescribeTagsInput) (*elbv2.DescribeTagsOutput, error)
}

type TagCacher interface {
	GetTags(string) (map[string]string, bool)
	PutTags(string, map[string]string)
}

type Kinesis interface {
	PutRecord(context.Context, *kinesis.PutRecordInput) (*kinesis.PutRecordOutput, error)
}

type LogFetcher interface {
	// Returns a LogOpener, time limit to process the file in and the error (if any) that was encountered while
	// attempting to fetch another log.
	GetLogOpeners(context.Context) ([]*LogOpener, time.Duration, error)
}

type LogParser interface {
	// Parses the logfile provider by the reader and places the results in the returned channel. Each element on the
	// channel is a buffered channel that will (eventually) contain a single ParseResult so it preserves ordering. Does
	// not closer the reader: this is the caller's responsibility.
	ParseReader(context.Context, io.Reader, *LogFileInfo) <-chan <-chan ParseResult
}

type LogLineParser interface {
	ParseLine(string) (*LogLineEntry, error)
}

type LogEntryWriter interface {
	// Pushes the provided LogEntry for writing.
	PushEntry(context.Context, *LogLineEntry) error

	// Write out all remaining buffered entries, if any.
	Flush(context.Context) error
}
