package logserver

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"
	"time"

	log "github.com/Sirupsen/logrus"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/sqs"
)

type SQSFetcher struct {
	S3Service             S3
	SqsService            SQS
	TagFetcherService     TagFetcher
	PollWaitTime          int64
	QueueUrl              string
	VisibilityTimeoutSecs int64
	MaxMessages           int64
}

type SnsMessage struct {
	Type    string `json:"Type"`
	Message string `json:"Message"`
}

// Event represents a S3 event with one or more records.
type Event struct {
	Records []EventRecord `json:"Records"`
}

// Record represents a single S3 record.
type EventRecord struct {
	EventName string `json:"eventName"`
	S3        struct {
		Bucket struct {
			Name string `json:"name"`
		} `json:"bucket"`
		Object struct {
			Key  string `json:"key"`
			Size int64  `json:"size"`
		} `json:"object"`
	} `json:"s3"`
}

func (f *SQSFetcher) GetLogOpeners(ctx context.Context) ([]*LogOpener, time.Duration, error) {
	receiveMessageParams := &sqs.ReceiveMessageInput{
		QueueUrl:            aws.String(f.QueueUrl),
		WaitTimeSeconds:     aws.Int64(f.PollWaitTime),
		VisibilityTimeout:   aws.Int64(f.VisibilityTimeoutSecs),
		MaxNumberOfMessages: aws.Int64(f.MaxMessages),
	}

	for {
		select {
		case <-ctx.Done():
			return nil, 0, ctx.Err()
		default:
		}

		resp, err := f.SqsService.ReceiveMessage(ctx, receiveMessageParams)
		if err != nil {
			log.Errorf("error receiving message: %v", err)
			continue
		}

		if len(resp.Messages) == 0 {
			continue
		}

		logOpeners := make([]*LogOpener, 0, len(resp.Messages))
		for _, message := range resp.Messages {
			if opener := f.parseMessage(message); opener != nil {
				logOpeners = append(logOpeners, opener)
			}
		}

		if len(logOpeners) == 0 {
			continue
		}
		return logOpeners, time.Duration(f.VisibilityTimeoutSecs) * time.Second, nil
	}
}

// Attempts to parse a given message, returning a LogOpener if successful, or nil otherwise.
func (f *SQSFetcher) parseMessage(message *sqs.Message) *LogOpener {
	var snsMessage SnsMessage
	if err := json.Unmarshal([]byte(*message.Body), &snsMessage); err != nil {
		// Cannot parse message.
		log.WithFields(log.Fields{
			"message_body": *message.Body,
		}).Warningf("cannot unmarshal message body: %v", err)
		// TODO: perhaps delete message from the queue since we'll never process it?
		return nil
	}

	if snsMessage.Type != "Notification" {
		log.WithFields(log.Fields{
			"message": fmt.Sprintf("%+v", snsMessage),
		}).Warningf("unexpected message not a notification")
		// TODO: perhaps delete message from the queue since we'll never process it?
		return nil
	}

	var event Event
	if err := json.Unmarshal([]byte(snsMessage.Message), &event); err != nil {
		log.WithFields(log.Fields{
			"sns_message": snsMessage.Message,
		}).Warningf("cannot unmarshal sns message: %v", err)
		// TODO: perhaps delete message from the queue since we'll never process it?
		return nil
	}

	opener := &LogOpener{
		SqsService:    f.SqsService,
		QueueUrl:      f.QueueUrl,
		ReceiptHandle: aws.StringValue(message.ReceiptHandle),
	}
	for _, record := range event.Records {
		if !strings.HasPrefix(record.EventName, "ObjectCreated") {
			log.WithFields(log.Fields{
				"record_event_name": record.EventName,
			}).Warning("ignoring record of incorrect type: only handling ObjectCreated events")
			continue
		}
		bucket := record.S3.Bucket.Name
		key := record.S3.Object.Key
		size := record.S3.Object.Size
		if size == 0 {
			log.WithFields(log.Fields{
				"bucket": bucket,
				"key":    key,
			}).Warning("ignoring new object of zero size")
			continue
		}
		if strings.HasSuffix(key, "ELBAccessLogTestFile") {
			log.WithFields(log.Fields{
				"bucket": bucket,
				"key":    key,
			}).Debug("ignoring test file")
			continue
		}
		opener.Openers = append(opener.Openers, S3LogFileOpener{
			bucket:     bucket,
			filename:   key,
			size:       size,
			s3Service:  f.S3Service,
			tagFetcher: f.TagFetcherService,
		})
	}
	return opener
}
