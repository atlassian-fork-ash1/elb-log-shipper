package logserver

import (
	"context"
	"errors"
	"sync"
	"testing"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/sqs"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type stubS3 struct {
	id string
}

func (s *stubS3) GetObject(context.Context, *s3.GetObjectInput) (*s3.GetObjectOutput, error) {
	return nil, nil
}

type mockSqsReceive struct {
	t                    *testing.T
	mutex                sync.Mutex
	numReceiveCalls      int
	expectedReceiveInput *sqs.ReceiveMessageInput
	receiveResponse      []*sqs.ReceiveMessageOutput
	receiveErr           []error
}

func (s *mockSqsReceive) ReceiveMessage(ctx context.Context, input *sqs.ReceiveMessageInput) (*sqs.ReceiveMessageOutput, error) {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	require.True(s.t, s.numReceiveCalls < len(s.receiveResponse), "too many calls to ReceiveMessage")
	require.Equal(s.t, s.expectedReceiveInput, input)
	s.numReceiveCalls++
	return s.receiveResponse[s.numReceiveCalls-1], s.receiveErr[s.numReceiveCalls-1]
}

func (s *mockSqsReceive) DeleteMessage(ctx context.Context, input *sqs.DeleteMessageInput) (*sqs.DeleteMessageOutput, error) {
	return nil, nil
}

func (s *mockSqsReceive) done() {
	assert.Equal(s.t, len(s.receiveResponse), s.numReceiveCalls)
}

func TestGetLogOpenerParsesReceivedMessages(t *testing.T) {
	t.Parallel()

	s3Service := &stubS3{id: "abc"}
	receiveInput := &sqs.ReceiveMessageInput{
		QueueUrl:            aws.String("queue"),
		WaitTimeSeconds:     aws.Int64(20),
		VisibilityTimeout:   aws.Int64(30),
		MaxNumberOfMessages: aws.Int64(10),
	}
	getOpenerTests := []struct {
		name               string
		s3Service          *stubS3
		sqsService         *mockSqsReceive
		expectedLogOpeners []*LogOpener
	}{
		{
			name:      "Retries erroneous receive",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					nil,
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"testbucket\" }, \"object\": { \"key\": \"testkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("receipt"),
							},
						},
					},
				},
				receiveErr: []error{errors.New("receive error"), nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{
						{bucket: "testbucket", filename: "testkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "receipt",
				},
			},
		},
		{
			name:      "Retries empty receive",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					{Messages: []*sqs.Message{}},
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"anotherbucket\" }, \"object\": { \"key\": \"anotherkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("receipt"),
							},
						},
					},
				},
				receiveErr: []error{nil, nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{
						{bucket: "anotherbucket", filename: "anotherkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "receipt",
				},
			},
		},
		{
			name:      "Processes multiple messages",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"bucketname\" }, \"object\": { \"key\": \"firstkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("first"),
							},
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"otherbucket\" }, \"object\": { \"key\": \"secondkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("second"),
							},
						},
					},
				},
				receiveErr: []error{nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{

						{bucket: "bucketname", filename: "firstkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "first",
				},
				{
					Openers: []S3LogFileOpener{
						{bucket: "otherbucket", filename: "secondkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "second",
				},
			},
		},
		{
			name:      "Ignores invalid JSON",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					{
						Messages: []*sqs.Message{
							{
								Body:          aws.String("invalid JSON"),
								ReceiptHandle: aws.String("invalid"),
							},
						},
					},
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"otherbucket\" }, \"object\": { \"key\": \"otherkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("receipt"),
							},
						},
					},
				},
				receiveErr: []error{nil, nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{
						{bucket: "otherbucket", filename: "otherkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "receipt",
				},
			},
		},
		{
			name:      "Only processes notifications",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "SubscriptionConfirmation",
                                    "Message": "Some messsage"
                                }`),
								ReceiptHandle: aws.String("invalid"),
							},
						},
					},
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"otherbucket\" }, \"object\": { \"key\": \"otherkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("receipt"),
							},
						},
					},
				},
				receiveErr: []error{nil, nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{
						{bucket: "otherbucket", filename: "otherkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "receipt",
				},
			},
		},
		{
			name:      "Ignore invalid S3 event",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ invalid event }"
                                }`),
								ReceiptHandle: aws.String("invalid"),
							},
						},
					},
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"otherbucket\" }, \"object\": { \"key\": \"otherkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("receipt"),
							},
						},
					},
				},
				receiveErr: []error{nil, nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{
						{bucket: "otherbucket", filename: "otherkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "receipt",
				},
			},
		},
		{
			name:      "Only processes S3 ObjectCreated events",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"abucket\" }, \"object\": { \"key\": \"akey\", \"size\": 10 } } }, { \"eventName\": \"ObjectRemoved:Delete\", \"s3\": { \"bucket\": { \"name\": \"onebucket\" }, \"object\": { \"key\": \"onekey\", \"size\": 10 } } }, { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"otherbucket\" }, \"object\": { \"key\": \"otherkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("receipt"),
							},
						},
					},
				},
				receiveErr: []error{nil, nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{
						{bucket: "abucket", filename: "akey", size: 10},
						{bucket: "otherbucket", filename: "otherkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "receipt",
				},
			},
		},
		{
			name:      "Ignore newly created objects with zero size",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"abucket\" }, \"object\": { \"key\": \"akey\", \"size\": 0 } } }, { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"otherbucket\" }, \"object\": { \"key\": \"otherkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("receipt"),
							},
						},
					},
				},
				receiveErr: []error{nil, nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{
						{bucket: "otherbucket", filename: "otherkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "receipt",
				},
			},
		},
		{
			name:      "Ignore ELB access log test file",
			s3Service: s3Service,
			sqsService: &mockSqsReceive{
				expectedReceiveInput: receiveInput,
				receiveResponse: []*sqs.ReceiveMessageOutput{
					{
						Messages: []*sqs.Message{
							{
								Body: aws.String(
									`{
                                    "Type": "Notification",
                                    "Message": "{ \"Records\": [ { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"abucket\" }, \"object\": { \"key\": \"somefolder/anotherfolder/ELBAccessLogTestFile\", \"size\": 105 } } }, { \"eventName\": \"ObjectCreated:Put\", \"s3\": { \"bucket\": { \"name\": \"abucket\" }, \"object\": { \"key\": \"otherkey\", \"size\": 10 } } } ] }"
                                }`),
								ReceiptHandle: aws.String("receipt"),
							},
						},
					},
				},
				receiveErr: []error{nil, nil},
			},
			expectedLogOpeners: []*LogOpener{
				{
					Openers: []S3LogFileOpener{
						{bucket: "abucket", filename: "otherkey", size: 10},
					},
					QueueUrl:      "queue",
					ReceiptHandle: "receipt",
				},
			},
		},
	}

	for _, test := range getOpenerTests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			test.sqsService.t = t
			for _, logOpener := range test.expectedLogOpeners {
				logOpener.SqsService = test.sqsService
				for i := range logOpener.Openers {
					logOpener.Openers[i].s3Service = test.s3Service
				}
			}
			fetcher := &SQSFetcher{
				S3Service:             test.s3Service,
				SqsService:            test.sqsService,
				PollWaitTime:          20,
				QueueUrl:              "queue",
				VisibilityTimeoutSecs: 30,
				MaxMessages:           10,
			}
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			logOpeners, timeout, err := fetcher.GetLogOpeners(ctx)
			require.NoError(t, err)
			if len(test.expectedLogOpeners) == 1 {
				assert.Equal(t, test.expectedLogOpeners, logOpeners)
			} else {
				assert.Equal(t, len(test.expectedLogOpeners), len(logOpeners))
			}
			assert.Equal(t, 30*time.Second, timeout)
			test.sqsService.done()
		})
	}
}

func TestGetLogOpenerExitsOnContext(t *testing.T) {
	t.Parallel()
	ctx, cancel := context.WithCancel(context.Background())
	cancel()

	fetcher := &SQSFetcher{
		PollWaitTime:          20,
		QueueUrl:              "queue",
		VisibilityTimeoutSecs: 30,
	}
	_, _, err := fetcher.GetLogOpeners(ctx)
	assert.Error(t, err)
}
