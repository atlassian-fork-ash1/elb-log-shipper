package logserver

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestExpiredKeysReturnNil(t *testing.T) {
	t.Parallel()

	cacher := NewLoadBalancerTagCacher()
	cacher.CleanupInterval = time.Millisecond * 100
	cacher.ExpirationTime = 0

	_, hasHello := cacher.GetTags("hello")
	require.False(t, hasHello)
	cacher.PutTags("hello", map[string]string{
		"company": "atlassian",
	})

	var tags map[string]string
	tags, hasHello = cacher.GetTags("hello")
	require.True(t, hasHello)
	require.Equal(t, map[string]string{
		"company": "atlassian",
	}, tags)
	time.Sleep(time.Millisecond * 150)

	_, hasHello = cacher.GetTags("hello")
	assert.False(t, hasHello)
}

func TestCanPutToEmptyCacher(t *testing.T) {
	t.Parallel()

	cacher := NewLoadBalancerTagCacher()
	cacher.CleanupInterval = time.Millisecond * 100
	cacher.ExpirationTime = 0

	cacher.PutTags("hello", map[string]string{
		"company": "atlassian",
	})

	tags, hasHello := cacher.GetTags("hello")
	require.True(t, hasHello)
	assert.Equal(t, map[string]string{
		"company": "atlassian",
	}, tags)
}
