package logserver

import (
	"context"
	"crypto/md5"
	"encoding/json"
	"errors"
	"strings"
	"testing"

	"github.com/aws/aws-sdk-go/service/kinesis"
	"github.com/golang/protobuf/proto"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

type mockKinesis struct {
	t                   *testing.T
	err                 error
	expectedNumRecords  []int
	expectedStreamName  string
	expectedLogEntryMap map[string]interface{}
	numCalls            int
}

func (k *mockKinesis) PutRecord(ctx context.Context, input *kinesis.PutRecordInput) (*kinesis.PutRecordOutput, error) {
	require.True(k.t, k.numCalls < len(k.expectedNumRecords))
	require.Equal(k.t, k.expectedStreamName, *input.StreamName)

	partitionKey := *input.PartitionKey
	data := string(input.Data)

	// Ensure message conforms to KPL aggregation format.
	require.True(k.t, strings.HasPrefix(data, kplMagicNumber))

	providedSum := data[len(data)-md5.Size:]
	message := []byte(data[len(kplMagicNumber) : len(data)-md5.Size])
	actualSum := md5.Sum(message)
	require.Equal(k.t, providedSum, string(actualSum[:]))

	var aggregatedRecord AggregatedRecord
	err := proto.Unmarshal(message, &aggregatedRecord)
	require.NoError(k.t, err)
	require.Equal(k.t, partitionKey, aggregatedRecord.PartitionKeyTable[0])
	require.Equal(k.t, k.expectedNumRecords[k.numCalls], len(aggregatedRecord.Records))

	for _, r := range aggregatedRecord.Records {
		require.Equal(k.t, uint64(0), *r.PartitionKeyIndex)
		if k.expectedLogEntryMap != nil {
			actualEntry := map[string]interface{}{}
			err := json.Unmarshal(r.Data, &actualEntry)
			require.NoError(k.t, err)
			require.Equal(k.t, k.expectedLogEntryMap, actualEntry)
		}
	}
	k.numCalls++

	return nil, k.err
}

func TestPushEntryCorrectlyAssemblesLogEntry(t *testing.T) {
	t.Parallel()

	logLineEntry := &LogLineEntry{
		ConnectionType: "connection",
	}
	logInfo := &LogFileInfo{
		LoadBalancer: LogEntryLoadBalancer{
			Name: "aloadbalancer",
		},
	}

	expectedLogEntryMap := makeLogEntryMap()
	expectedLogEntryMap["connection_type"] = "connection"
	if expectedLoadBalancerMap, ok := expectedLogEntryMap["load_balancer"].(map[string]interface{}); ok {
		expectedLoadBalancerMap["name"] = "aloadbalancer"
	}
	expectedLogEntryMap["env"] = "ddev"
	expectedLogEntryMap["serviceId"] = "serviceid"
	expectedLogEntryMap["sourcetype"] = "sourcetype"

	kinesisService := &mockKinesis{
		t:                   t,
		err:                 nil,
		expectedNumRecords:  []int{1},
		expectedStreamName:  "stream",
		expectedLogEntryMap: expectedLogEntryMap,
	}
	writer := &KinesisWriter{
		KinesisService:    kinesisService,
		LogStreamName:     "stream",
		Environment:       "ddev",
		ServiceId:         "serviceid",
		SourceType:        "sourcetype",
		LogInfo:           logInfo,
		MaxRecordDataSize: 300 * 1024,
		MaxShardRetries:   1,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	require.NoError(t, writer.PushEntry(ctx, logLineEntry))
	require.NoError(t, writer.Flush(ctx))
	assert.Equal(t, 1, kinesisService.numCalls)
}

func TestPushEntryCorrectlyAssemblesLogEntryWithLogEntryCustomiser(t *testing.T) {
	t.Parallel()

	logLineEntry := &LogLineEntry{
		ConnectionType: "connection",
	}
	logInfo := &LogFileInfo{
		LoadBalancer: LogEntryLoadBalancer{
			Name: "aloadbalancer",
		},
	}

	expectedLogEntryMap := makeLogEntryMap()
	expectedLogEntryMap["connection_type"] = "connection"
	if expectedLoadBalancerMap, ok := expectedLogEntryMap["load_balancer"].(map[string]interface{}); ok {
		expectedLoadBalancerMap["name"] = "aloadbalancer"
	}
	expectedLogEntryMap["env"] = "ddev"
	expectedLogEntryMap["serviceId"] = "serviceid"
	expectedLogEntryMap["sourcetype"] = "sourcetype"

	// new property per CustomLogEntry
	expectedLogEntryMap["custom_tag"] = "custom-value"

	kinesisService := &mockKinesis{
		t:                   t,
		err:                 nil,
		expectedNumRecords:  []int{1},
		expectedStreamName:  "stream",
		expectedLogEntryMap: expectedLogEntryMap,
	}
	writer := &KinesisWriter{
		KinesisService:    kinesisService,
		LogStreamName:     "stream",
		Environment:       "ddev",
		ServiceId:         "serviceid",
		SourceType:        "sourcetype",
		LogInfo:           logInfo,
		MaxRecordDataSize: 300 * 1024,
		MaxShardRetries:   1,
		LogEntryCustomiser: func(logEntry LogEntry) interface{} {
			type CustomLogEntry struct {
				LogEntry
				CustomTag string `json:"custom_tag,omitempty"`
			}
			customLogEntry := CustomLogEntry{
				logEntry,
				"custom-value",
			}

			return customLogEntry
		},
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	require.NoError(t, writer.PushEntry(ctx, logLineEntry))
	require.NoError(t, writer.Flush(ctx))
	assert.Equal(t, 1, kinesisService.numCalls)
}

func TestPushRecordPartitionsRecordsCorrectly(t *testing.T) {
	t.Parallel()
	kinesisService := &mockKinesis{
		t:                  t,
		err:                nil,
		expectedNumRecords: []int{1, 3, 1},
		expectedStreamName: "stream",
	}
	writer := &KinesisWriter{
		KinesisService: kinesisService,
		LogStreamName:  "stream",
		LogInfo: &LogFileInfo{
			LoadBalancer: LogEntryLoadBalancer{
				Name: "loadbalancer",
			},
		},
		MaxRecordDataSize: 10,
		MaxShardRetries:   1,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	dataStrings := []string{
		"1234567",
		"1234",
		"12345",
		"1",
		"123",
	}

	for _, d := range dataStrings {
		require.NoError(t, writer.pushRecord(ctx, &Record{
			Data:              []byte(d),
			PartitionKeyIndex: proto.Uint64(0),
		}))
	}
	require.NoError(t, writer.Flush(ctx))
	assert.Equal(t, 3, kinesisService.numCalls)
}

func TestFlushRetriesErrors(t *testing.T) {
	t.Parallel()
	kinesisService := &mockKinesis{
		t:                  t,
		err:                errors.New("write error"),
		expectedNumRecords: []int{1, 1, 1, 1, 1, 1},
		expectedStreamName: "stream",
	}
	writer := &KinesisWriter{
		KinesisService: kinesisService,
		LogStreamName:  "stream",
		LogInfo: &LogFileInfo{
			LoadBalancer: LogEntryLoadBalancer{
				Name: "loadbalancer",
			},
		},
		MaxRecordDataSize: 10,
		MaxShardRetries:   3,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	require.NoError(t, writer.pushRecord(ctx, &Record{
		Data:              []byte("123456"),
		PartitionKeyIndex: proto.Uint64(0),
	}))
	require.Error(t, writer.pushRecord(ctx, &Record{
		Data:              []byte("12345"),
		PartitionKeyIndex: proto.Uint64(0),
	}))
	require.Error(t, writer.Flush(ctx))
	assert.Equal(t, 6, kinesisService.numCalls)
}

func TestPushRecordRejectsTooLargeRecords(t *testing.T) {
	t.Parallel()
	writer := &KinesisWriter{
		LogInfo: &LogFileInfo{
			LoadBalancer: LogEntryLoadBalancer{
				Name: "loadbalancer",
			},
		},
		MaxRecordDataSize: 1,
		MaxShardRetries:   1,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	assert.Error(t, writer.pushRecord(ctx, &Record{
		Data:              []byte("12345"),
		PartitionKeyIndex: proto.Uint64(0),
	}))
}

func TestFlushNopOnEmptyBuffer(t *testing.T) {
	t.Parallel()
	kinesisService := &mockKinesis{
		t:                  t,
		err:                nil,
		expectedNumRecords: []int{1},
		expectedStreamName: "stream",
	}
	writer := &KinesisWriter{
		KinesisService: kinesisService,
		LogStreamName:  "stream",
		LogInfo: &LogFileInfo{
			LoadBalancer: LogEntryLoadBalancer{
				Name: "loadbalancer",
			},
		},
		MaxRecordDataSize: 300 * 1024,
		MaxShardRetries:   3,
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	require.NoError(t, writer.Flush(ctx))
	require.NoError(t, writer.PushEntry(ctx, &LogLineEntry{}))
	require.NoError(t, writer.Flush(ctx))
	require.NoError(t, writer.Flush(ctx))
	assert.Equal(t, 1, kinesisService.numCalls)
}

// makeLogEntryMap produces a map based on the types in ./logserver.go
func makeLogEntryMap() map[string]interface{} {
	// see: LogEntry
	return map[string]interface{}{
		// see: LogFileInfo
		"load_balancer": map[string]interface{}{
			// see: LogEntryLoadBalancer
			"aws_account_id": "",
			"name":           "",
			"type":           "",
			"ip":             "",
			"region":         "",
		},
		"log_bucket":   "",
		"log_filename": "",

		// see: LogLineEntry
		"connection_type":          "",
		"time":                     "",
		"src_ip":                   "",
		"dest_ip":                  "",
		"request_processing_time":  float64(0),
		"target_processing_time":   float64(0),
		"response_processing_time": float64(0),
		"bytes_in":                 float64(0),
		"bytes_out":                float64(0),
		"request":                  map[string]interface{}{"client": ""},
		"http_user_agent":          "",

		// see: LogEntry
		"env":        "",
		"serviceId":  "",
		"sourcetype": "",
	}
}
